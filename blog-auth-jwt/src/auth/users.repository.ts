import { User } from './user.entity';
import { EntityRepository, Repository } from 'typeorm';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { Role } from './role-enum';

@EntityRepository(User)
export class UsersRepository extends Repository<User> {
  async createUser(authCredentialsDto: AuthCredentialsDto): Promise<void> {
    const { username, password } = authCredentialsDto;

    // hash
    const salt = await bcrypt.genSaltSync();
    const hashPassword = await bcrypt.hash(password, salt);

    const user = await this.create({
      username,
      password: hashPassword,
      /**
       * create default role user
       * notice this just for example code. // (change later!)
       */
      // email: 'Tahr@example.com',
      role: Role.USER,
    });
    try {
      await this.save(user);
    } catch (error) {
      /**
       * Error code 23505`
       * https://www.postgresql.org/docs/9.2/errcodes-appendix.html
       * Error duplicate
       */
      console.log(error);
      if (error.code === '23505') {
        throw new ConflictException('Username already exists');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }
}
