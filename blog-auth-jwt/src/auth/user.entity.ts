import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { v4 as uuid } from 'uuid';
import { Role } from './role-enum';
import { Blog } from '../blog/blog.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn(uuid)
  id: string;

  @Column({ unique: true })
  username: string;

  @Column({ nullable: true })
  email: string;

  @Column()
  password: string;

  @Column()
  role: Role;

  @OneToMany((_type) => Blog, (blog) => blog.user, { eager: true })
  post: Blog[];
}
