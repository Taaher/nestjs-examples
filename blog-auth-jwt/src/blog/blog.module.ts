import { Module } from '@nestjs/common';
import { BlogService } from './blog.service';
import { BlogController } from './blog.controller';
import { AuthModule } from 'src/auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlogsRepository } from './blogs.repository';

@Module({
  imports: [TypeOrmModule.forFeature([BlogsRepository]), AuthModule],
  providers: [BlogService],
  controllers: [BlogController],
})
export class BlogModule {}
