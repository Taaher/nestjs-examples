import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  UseGuards,
  Query,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BlogService } from './blog.service';
import { Blog } from './blog.entity';
import { CreatePostDto } from './dto/createPost.dto';
import { UpdatePostDto } from './dto/updatePost.dto';
import { GetUser } from '../auth/get-user.decorator';
import { User } from '../auth/user.entity';
import { Logger } from '@nestjs/common';
import { query } from 'express';
import { GetPostFilterDto } from './dto/getPostFilter.dto';

@Controller('blog')
export class BlogController {
  private logger = new Logger('BlogController');
  constructor(private blogService: BlogService) {}

  @Get()
  // @UseGuards(AuthGuard())
  getPosts(
    @Query() getPostFilterDto: GetPostFilterDto,
    @GetUser()
    user: User,
  ): Promise<Blog[]> {
    // this.logger.verbose(`User ${user.username}`);
    console.log(user);
    return this.blogService.getPosts(getPostFilterDto, user);
  }

  @Get(':id')
  getPostById(@Param('id') id: string, @GetUser() user: User): Promise<Blog> {
    return this.blogService.getPostById(id, user);
  }

  @Post()
  @UseGuards(AuthGuard())
  createPost(
    @Body() createPostDto: CreatePostDto,
    @GetUser() user: User,
  ): Promise<Blog> {
    return this.blogService.createPost(createPostDto, user);
  }

  @Put(':id')
  @UseGuards(AuthGuard())
  updatePost(
    @Param('id') id: string,
    @Body() updatePostDto: UpdatePostDto,
    @GetUser() user: User,
  ) {
    return this.blogService.updatePost(id, updatePostDto, user);
  }

  @Delete(':id')
  @UseGuards(AuthGuard())
  deletePost(@Param('id') id: string, @GetUser() user: User): Promise<void> {
    return this.blogService.deletePost(id, user);
  }
}
