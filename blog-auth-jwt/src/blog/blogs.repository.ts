import { User } from 'src/auth/user.entity';
import { EntityRepository, Repository } from 'typeorm';
import { PostPublished } from './blog-status-enum';
import { Blog } from './blog.entity';
import { CreatePostDto } from './dto/createPost.dto';
import { GetPostFilterDto } from './dto/getPostFilter.dto';
import { AuthService } from '../auth/auth.service';
import { InjectRepository } from '@nestjs/typeorm';

@EntityRepository(Blog)
export class BlogsRepository extends Repository<Blog> {
  constructor(
    @InjectRepository(User)
    private authService: AuthService,
  ) {
    super();
  }
  /**
   *
   */
  async getPosts(
    getPostFilterDto: GetPostFilterDto,
    user: User,
  ): Promise<Blog[]> {
    console.log(user);
    const { search, username } = getPostFilterDto;
    const query = this.createQueryBuilder('blog');
    if (user) {
      query.where({ user });
    }

    if (username) {
      console.log(username);

      query.andWhere('user.username (:username)', {
        username: `%${username}%`,
      });
    }

    if (search) {
      query.andWhere(
        'LOWER(blog.title) LIKE LOWER(:search) OR LOWER(blog.body) LIKE LOWER(:search)',
        { search: `%${search}%` },
      );
    }

    const posts = await query.getMany();
    return posts;
  }
  /**
   *
   */
  async createPost(createPostDto: CreatePostDto, user: User): Promise<Blog> {
    const { title, body } = createPostDto;

    const post = await this.create({
      title,
      body,
      published: PostPublished.DRAFT,
      user,
    });

    return this.save(post);
  }
}
