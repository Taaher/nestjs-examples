import { IsEnum, IsString } from 'class-validator';
import { PostPublished } from '../blog-status-enum';

export class CreatePostDto {
  @IsString()
  title: string;

  @IsString()
  body: string;

  @IsEnum(PostPublished)
  published: string;
}
