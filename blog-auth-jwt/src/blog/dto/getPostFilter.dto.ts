import { IsOptional, IsString } from 'class-validator';
import { User } from '../../auth/user.entity';

export class GetPostFilterDto {
  @IsOptional()
  @IsString()
  username?: User;

  @IsOptional()
  @IsString()
  search?: string;
}
