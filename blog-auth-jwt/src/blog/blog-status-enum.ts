export enum PostPublished {
  PUBLISH = 'PUBLISH',
  DRAFT = 'DRAFT',
}
