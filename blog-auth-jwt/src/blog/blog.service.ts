import { Injectable, NotFoundException } from '@nestjs/common';
import { BlogsRepository } from './blogs.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Blog } from './blog.entity';
import { CreatePostDto } from './dto/createPost.dto';
import { UpdatePostDto } from './dto/updatePost.dto';
import { User } from '../auth/user.entity';
import { GetPostFilterDto } from './dto/getPostFilter.dto';

@Injectable()
export class BlogService {
  constructor(
    @InjectRepository(BlogsRepository)
    private blogsRepository: BlogsRepository,
  ) {}
  /**
      Get all Posts
    */
  async getAllPosts() {
    //
  }

  async getPosts(
    getPostFilterDto: GetPostFilterDto,
    user: User,
  ): Promise<Blog[]> {
    return await this.blogsRepository.getPosts(getPostFilterDto, user);
  }

  async getPostById(id: string, user: User): Promise<Blog> {
    const found = await this.blogsRepository.findOne({ where: { id, user } });
    if (!found) {
      throw new NotFoundException(`Not Found ${id}`);
    }
    return found;
  }

  async createPost(createPostDto: CreatePostDto, user: User): Promise<Blog> {
    return await this.blogsRepository.createPost(createPostDto, user);
  }

  async updatePost(
    id,
    updatePostDto: UpdatePostDto,
    user: User,
  ): Promise<Blog> {
    const post = await this.getPostById(id, user);
    const { title, body } = updatePostDto;
    post.title = title;
    post.body = body;
    return await this.blogsRepository.save(post);
  }

  async deletePost(id: string, user: User): Promise<void> {
    const found = await this.getPostById(id, user);
    await this.blogsRepository.delete(found.id);
  }
}
