import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { v4 as uuid } from 'uuid';
import { PostPublished } from './blog-status-enum';
import { User } from '../auth/user.entity';
import { Exclude } from 'class-transformer';

@Entity()
export class Blog {
  @PrimaryGeneratedColumn(uuid)
  id: string;

  @Column()
  title: string;

  @Column()
  body: string;

  @Column()
  published: PostPublished;

  @ManyToOne((_type) => User, (user) => user.post, { eager: false })
  // @Exclude({ toPlainOnly: true })
  user: User;
}
