import { Module } from '@nestjs/common';
import { TaskModule } from './task/task.module';
import { MongooseModule } from '@nestjs/mongoose';
@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/nestjs_mongodb', {
      useFindAndModify: false,
    }),
    TaskModule,
  ],
})
export class AppModule {}
