export class UpdateTaskDto {
  title: string;
  description: string;
  body: string;
}
