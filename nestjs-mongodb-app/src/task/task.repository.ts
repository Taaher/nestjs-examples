import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Task, TaskDocument } from './schemas/task.schema';
import { Model } from 'mongoose';
import { CreateTaskDto } from './dtos/create-task.dto';
import { UpdateTaskDto } from './dtos/update-task.dto';

@Injectable()
export class TaskRepository {
  constructor(@InjectModel(Task.name) private TaskModel: Model<TaskDocument>) {}
  //get all tasks
  async getTasks(): Promise<Task[]> {
    return await this.TaskModel.find();
  }

  //get one task By id
  async getTask(id): Promise<Task> {
    const task = await this.TaskModel.findById(id);
    if (!task) {
      throw new NotFoundException(`Not Found ID : ${id}`);
    }
    return task;
  }
  //create task
  async createTask(createTaskDto: CreateTaskDto): Promise<Task> {
    const newTask = await this.TaskModel.create(createTaskDto);
    return newTask;
  }
  // update task
  async updateTask(id: string, updateTaskDto: UpdateTaskDto): Promise<Task> {
    const task = await await this.TaskModel.findOneAndUpdate(
      { _id: id },
      updateTaskDto,
      { new: true },
    );
    if (!task) {
      throw new NotFoundException(`Not Found ID : ${id} `);
    }
    return task;
  }

  async deleteTask(id: string) {
    const task = await this.getTask(id);
    if (!task) {
      throw new NotFoundException();
    }
    return await this.TaskModel.findOneAndDelete({ _id: id });
  }
}
