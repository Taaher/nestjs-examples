import { Injectable } from '@nestjs/common';
import { CreateTaskDto } from './dtos/create-task.dto';
import { UpdateTaskDto } from './dtos/update-task.dto';
import { TaskRepository } from './task.repository';

@Injectable()
export class TaskService {
  constructor(private taskRepository: TaskRepository) {}

  getTasks() {
    return this.taskRepository.getTasks();
  }

  getTask(id) {
    return this.taskRepository.getTask(id);
  }

  createTask(createTaskDto: CreateTaskDto) {
    return this.taskRepository.createTask(createTaskDto);
  }

  updateTask(id: string, updateTaskDto: UpdateTaskDto) {
    return this.taskRepository.updateTask(id, updateTaskDto);
  }

  deleteTask(id: string) {
    return this.taskRepository.deleteTask(id);
  }
}
