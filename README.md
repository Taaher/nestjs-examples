# nestjs-examples

### **Convert app nodejs to Nestjs**


| Project | Feature | Testing | Auth | Database |
| ------ | ------ | ------ | ------ |  ------ |
| task-manager | **Nestjs api** | ❌ | ❌  |
| blog | Api - **Auth** | typing...✅ | Jwt✅ | PostgreSQL|
| Nest-mongodb-app | Mongodb |  ❌  |  ❌  | **Mongodb** |


